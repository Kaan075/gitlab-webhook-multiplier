import express from "express";
import bodyParser from "body-parser";
import axios from "axios";

export = class Client {
    private config: any;
    private app: any;
    constructor(config: any) {
        this.config = config;
        this.app;
    }

    public init() {
        this.app = express();

        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());

        this.app.listen(this.config.port, () => {
            this.run();
            console.log(`Running on port ${this.config.port}`);
        });
    }

    public async run() {
        this.app.get("/", async (req: any, res: any) => {
            res.sendStatus(202);
        });

        this.app.post("/", async (req: any, res: any) => {
            console.log(req.body);
            this.config.links.map((url: string) => {
                axios.post(url, req.body.payload);
            }).catch((err: any) => {
                console.log(err);
            });
        });
    }
}